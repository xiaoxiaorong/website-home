import '@/styles/tailwind.css'
import 'focus-visible'
import React from "react";

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}
